##########   Imports   ##########
import tensorflow as tf
import numpy as np
import zmq
import struct
import sys

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:50000")
##########   Util Functions   ##########
def _recv_tensor_():
	data = socket.recv()
	socket.send(b"\0")
	p = [0]
	def get(i, n):
		if n == 0:
			return ()
		f = "!%d%s" % (n, i)
		r = struct.unpack(f, data[p[0]:p[0]+4*n])
		p[0] += 4 * n
		return r
	data_map = dict()
	total = get('i', 1)[0]
	while total > 0:
		total -= 1
		code, idn, dtype, dim = get('i', 4)
		dtype = "if"[dtype]
		shape = get('i', dim)
		size = reduce(lambda x,y:x*y, shape + (1,))
		num = get('i', 1)[0]
		while num > 0:
			num -= 1
			idx = get('i', idn)
			t = np.array(get(dtype, size), dtype={'i':np.int32,'f':np.float32}[dtype]).reshape(shape)
			data_map[(code&0xffffffff,)+idx] = t
	return data_map
def send_tensor(name, index, value):
	pass
def _hash_(s):
	base = 137
	now = 1
	code = 0
	for c in s:
		code += ord(c) * now
		code &= 0xffffffff
		now *= base
	return code
_zero_map_ = dict()
def _zeros_(s):
	if not _zero_map_.has_key(s):
		if s[-1] == 0:
			dtype = tf.int32
		else:
			dtype = tf.float32
		shape = s[0:-1]
		if len(shape) == 3:
			shape = (shape[0],shape[2])
		_zero_map_[s] = tf.zeros(shape, dtype)
	return _zero_map_[s]
##########   Variables   ##########
need_feed = []
trained = []
global_data = _recv_tensor_()
P_map_ = dict()
def P():
	if not P_map_.has_key(()):
		P_map_[()] = tf.Variable(tf.random_normal([6040,200], mean=0.000000, stddev=0.100000), trainable=True)
		trained.append(('P', (), P_map_[()]))
	return P_map_[()]
Q_map_ = dict()
def Q():
	if not Q_map_.has_key(()):
		Q_map_[()] = tf.Variable(tf.random_normal([3952,200], mean=0.000000, stddev=0.100000), trainable=True)
		trained.append(('Q', (), Q_map_[()]))
	return Q_map_[()]
V_map_ = dict()
def V():
	if not V_map_.has_key(()):
		_t = global_data[(_hash_('V'),)+()]
		V_map_[()] = tf.Variable(_t, trainable=False)
	return V_map_[()]
##########   Computations   ##########
T_3 = V()
T_5 = P()
T_7 = Q()
T_6 = tf.transpose(T_7)
T_4 = tf.matmul(T_5, T_6)
T_2 = tf.sub(T_3, T_4)
T_1 = tf.square(T_2)
T_0 = tf.reduce_sum(T_1)
##########   Train   ##########
_target_ = T_0
_train_ = tf.train.AdamOptimizer(0.01).minimize(_target_)
sess = tf.Session()
sess.run(tf.initialize_all_variables())
print 'train'
sys.stdout.flush()
from time import time
for epoch in range(1000):
	_a_=time()
	_,_value_ = sess.run([_train_, _target_])
	_b_=time()
	print epoch, '/ 1000', ':  ',  _value_, 'calc', _b_-_a_
	sys.stdout.flush()
##########   Send Result   ##########
for (s, i, t) in trained:
	tensor = sess.run(t)
	send_tensor(s, i, tensor)
