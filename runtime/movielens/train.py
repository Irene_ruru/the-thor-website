#!/usr/bin/python

import numpy as np
import tensorflow as tf

data_path = "ratings.dat"

N = 0
M = 0
R = 100
data = []

def read_data():
	global data, N, M
	min_x = 1000000 
	min_y = 1000000
	max_x = 0
	max_y = 0
	sparse = []
	f = open(data_path)
	for line in f:
		l = line.strip().split('::')
		x, y, v = l[:3]
		x = int(x)
		y = int(y)
		v = float(v)
		sparse.append((x, y, v))
		min_x = min(x, min_x)
		max_x = max(x, max_x)
		min_y = min(y, min_y)
		max_y = max(y, max_y)
		
	N = max_x - min_x + 1
	M = max_y - min_y + 1
	data = np.zeros((N, M))
	for x, y, v in sparse:
		data[x-min_x][y-min_y] = v / 5.0 - 0.5
		
def matrix_completion():
	global data, N, M
	print N, M
	V = tf.constant(data, dtype=tf.float32)
	P = tf.Variable(tf.random_normal((N, R), mean=0.0, stddev=0.2))
	Q = tf.Variable(tf.random_normal((M, R), mean=0.0, stddev=0.2))
	T = tf.matmul(P, tf.transpose(Q))
	loss = tf.reduce_sum(tf.square(V - T))

	train = tf.train.AdamOptimizer().minimize(loss)

	sess = tf.Session()
	sess.run(tf.initialize_all_variables())

	while True:
		_, l = sess.run([train, loss])
		print l

def main():
	read_data()
	matrix_completion()

if __name__ == '__main__':
	main()
